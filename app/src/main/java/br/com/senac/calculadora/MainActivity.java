package br.com.senac.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private String op;

    private double numero1;
    private double numero2;
    private double resultado;

    private boolean limpar = true;

    private TextView display;
    private String texto;

    private Button bnt01 ;
    private Button bnt02 ;
    private Button bnt03 ;
    private Button bnt04 ;
    private Button bnt05 ;
    private Button bnt06 ;
    private Button bnt07 ;
    private Button bnt08 ;
    private Button bnt09 ;
    private Button bnt00 ;
    private Button bntigual ;
    private Button bntmultiplicar;
    private Button bntdividir;
    private Button bntsomar;
    private Button bntsubtrair;
    private Button bntCe;
    private Button bntponto;

    public void inic(){

        display = findViewById(R.id.display);

        bnt01 = findViewById(R.id.um);
        bnt02 = findViewById(R.id.dois);
        bnt03 = findViewById(R.id.tres);
        bnt04 = findViewById(R.id.quatro);
        bnt05 = findViewById(R.id.cinco);
        bnt06 = findViewById(R.id.seis);
        bnt07 = findViewById(R.id.sete);
        bnt08 = findViewById(R.id.oito);
        bnt09 = findViewById(R.id.nove);
        bnt00 = findViewById(R.id.zero);
        bntigual = findViewById(R.id.igual);
        bntmultiplicar = findViewById(R.id.multiplicar);
        bntdividir = findViewById(R.id.dividir);
        bntsomar = findViewById(R.id.somar);
        bntsubtrair = findViewById(R.id.subtrair);
        bntCe = findViewById(R.id.CE);
        bntponto = findViewById(R.id.ponto);

        ///display.setText("0");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inic();

        bntsomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { pegar();       op = "+";
             }
        });
        bntsubtrair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { pegar();    op = "-";
            }
        });
        bntmultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { pegar(); op = "*";;
            }
        });
        bntdividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { pegar();     op = "/";
            }
        });

        bntigual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { calculo(op);   limpar = true;

            }
        });

        bntCe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { display.setText("0");   limpar = true;

            }
        });

        bntponto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                texto = display.getText().toString();
                if (!texto.contains(".")) {
                    display.setText(texto + ".");
                    limpar = false;
                }

            }
        });


        bnt00.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            inserir(bnt00);
            }
        });
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt01);
            }
        });
        bnt02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt02);
            }
        });
        bnt03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt03);
            }
        });
        bnt04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt04);
            }
        });
        bnt05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt05);
            }
        });
        bnt06.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt06);
            }
        });
        bnt07.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt07);
            }
        });
        bnt08.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt08);
            }
        });
        bnt09.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserir(bnt09);
            }
        });



    }

    private void pegar() {
        numero1 = Double.parseDouble(display.getText().toString());
        limpar = true;
    }

    public void inserir(Button button){
        String texto = button.getText().toString();

        if(limpar){
            display.setText(texto);
            limpar = false;
        }else{
            String textoDisplay = display.getText().toString() + texto;
            display.setText(textoDisplay);

        }


    }

public void calculo(String op){
    if (op != null) {
        switch (op) {
            case "+":
                numero2 = Double.parseDouble(display.getText().toString());
                resultado = numero1 + numero2;
                break;
            case "-":
                numero2 = Double.parseDouble(display.getText().toString());
                resultado = numero1 - numero2;
                break;
            case "*":
                numero2 = Double.parseDouble(display.getText().toString());
                resultado = numero1 * numero2;
                break;
            case "/":
                numero2 = Double.parseDouble(display.getText().toString());
                resultado = numero1 / numero2;
                break;
        }

        display.setText(String.valueOf(resultado));
    }
}



}
